var mongoose = require("mongoose");
var Campground = require("./models/campground");
var Comment = require("./models/comment");

var data = [
    {
        name: "Clouds Rest",
        image: "https://images.pexels.com/photos/618848/pexels-photo-618848.jpeg?h=350&auto=compress&cs=tinysrgb",
        description: "Nice lights outside"
    },
    {
        name: "Desert Mesa",
        image: "https://images.pexels.com/photos/216675/pexels-photo-216675.jpeg?h=350&auto=compress&cs=tinysrgb",
        description: "Breathtaking views"
    },
    {
        name: "Silver Dunes",
        image: "https://images.pexels.com/photos/216676/pexels-photo-216676.jpeg?h=350&auto=compress&cs=tinysrgb",
        description: "Amazing campground site"
    }
]
function seedDB(){
    //Remove All Campgrounds
    Campground.remove({}, function(err){
        if(err){
            console.log(err);
        }
        console.log("REMOVED CAMPGROUNDS");
        data.forEach(function(seed){
            Campground.create(seed, function(err, campground){
                if(err){
                    console.log(err);
                } else {
                    console.log("Added a campground..")
                    //Create a comment
                    Comment.create(
                        {
                            text: "This place is great but I wish there was internet",
                            author: "Homer"
                        }, function(err, comment){
                            if(err){
                                console.log(err)
                        } else {
                            campground.comments.push(comment._id);
                            campground.save();
                            console.log("Created new comment");
                        }
                    });
                }
            });
        });
    });
    //Add A Few Campgrounds
}

module.exports = seedDB;